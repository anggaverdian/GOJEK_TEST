
$(window).on('resize', function(){
    var win = $(this);
      if (win.width() > 460) {
        $(".arrow-down").addClass('hide');
        $(".arrow-up").removeClass('show');
        $(".arrow-down").removeClass('show');
        $(".collapse ul li").show();
      }
      else {
        $(".arrow-down").removeClass('hide');
        $(".arrow-down").addClass('show');
        $(".sub").hide();
      }
});

$(document).ready(function(){

    $(".go-jek").click(function(){

        if($(window).width() <= 460){

            $(".go-jek-submenu").toggle();

            if ($(".go-jek .arrow-up").css("display") === 'none') {

                $(".go-jek .arrow-down").addClass('hide');
                $(".go-jek .arrow-up").addClass('show');

            } else {

                $(".go-jek .arrow-up").removeClass('show');
                $(".go-jek .arrow-down").removeClass('hide');
            }
        }

    });

    $(".go-life").click(function(){

        if($(window).width() <= 460){
            $(".go-life-submenu").toggle();

            if ($(".go-life .arrow-up").css("display") === 'none') {

                $(".go-life .arrow-down").addClass('hide');
                $(".go-life .arrow-up").addClass('show');

            } else {

                $(".go-life .arrow-up").removeClass('show');
                $(".go-life .arrow-down").removeClass('hide');
            }
        }
    });

    $(".go-pay").click(function(){

        if($(window).width() <= 460){
            $(".go-pay-submenu").toggle();

            if ($(".go-pay .arrow-up").css("display") === 'none') {

                $(".go-pay .arrow-down").addClass('hide');
                $(".go-pay .arrow-up").addClass('show');

            } else {

                $(".go-pay .arrow-up").removeClass('show');
                $(".go-pay .arrow-down").removeClass('hide');
            }
        }
    });
});